


FTemplate = FLUIDTEMPLATE
FTemplate {
  file = fileadmin/templates/private/welcome.html
  layoutRootPath = fileadmin/templates/private/layouts/
  partialRootPath = fileadmin/templates/private/partials/



  variables {


    menu < nav
    brot < brot




    welcome < styles.content.get
    welcome.select.where = colPos=0


    quote < styles.content.get
    quote.select.where = colPos=1

  }

}