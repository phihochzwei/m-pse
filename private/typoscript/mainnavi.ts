

brot = HMENU
brot{
    special = rootline

    1 = TMENU
    1{
        NO = 1
        NO.stdWrap2.wrap = |<span class="brotsep">&bull;</span>
        CUR = 1
        CUR < .NO
        CUR.ATagParams = class="brotact"
        ACT = 1
        ACT < .CUR
    }
}


nav = COA
nav {
    wrap = <ul>|</ul>

    20 = HMENU
    20{

        1 = TMENU
        1 {
            NO = 1
            NO.stdWrap2.wrap = <li>|</li>
            CUR = 1
            CUR < .NO
            CUR.ATagParams = class="act"
            ACT = 1
            ACT < .CUR
        }
    }
}

