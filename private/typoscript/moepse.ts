



moepse = HMENU

moepse {
    wrap = <ul>|</ul>
    special = directory
    special.value = 3
    1 = TMENU
    1 {
        NO = 1
        NO.stdWrap2.wrap = <li>|</li>
        CUR = 1
        CUR < .NO
        CUR.ATagParams = class="act"
        ACT = 1
        ACT < .CUR
    }
}



FTemplate = FLUIDTEMPLATE
FTemplate {
  file = fileadmin/templates/private/moepse.html
  layoutRootPath = fileadmin/templates/private/layouts/
  partialRootPath = fileadmin/templates/private/partials/



  variables {
    /*
    welcome < styles.content.get
    welcome.select.where = colPos=0


    quote < styles.content.get
    quote.select.where = colPos=1
    */

    menu < nav
    brot < brot

    submenu < moepse
  }

}